# Spring整合Redis

```text
Redis在JavWeb中的应用，一般而言Redis在java Web中映红存在两个主要的场景，
一个是缓存常用的数据，另一个是在需要高速读写的场合使用，比如一些需要进行商品
抢购和抢红包的场合。
```

## Redis的javaAPI
 
 - 在java程序中使用Redis
 ```text
在java中使用Redis工具，需要先下载jedis.jar包，把它加载到工程路径中，
所以需要先下载jar包或者使用maven管理工具，在pom文件中引入jar包。
```
```xml
 <!-- https://mvnrepository.com/artifact/redis.clients/jedis -->
        <dependency>
            <groupId>redis.clients</groupId>
            <artifactId>jedis</artifactId>
            <version>2.9.0</version>
        </dependency>
```
  ### 1）java连接Redis的demo，测试Redis在当前环境中的读写速度
 ```java
package cn.qzbook.test;

import redis.clients.jedis.Jedis;

/**
 * @Description
 * @Author zc
 * @Date 2020/11/2 下午7:40
 */
public class JedisTest {

    public static void main(String[] args) {
        //连接地址，端口
        Jedis jedis = new Jedis("192.168.0.120",6379);
        
        int i = 0;
        try {
            //开始时间
            long start = System.currentTimeMillis();
            while (true){
                //结束时间
                long end = System.currentTimeMillis();
                if (end - start >= 1000){
                    break;
                }
                i++;
                jedis.set("test"+i,i+"");
            }
        }finally {
            jedis.close();
        }

        System.out.println("redis每秒操作："+i+"次");
    }
}
```
  ### 2）使用Redis连接池
  ```java
public class JedisPoolTest {

    public static void main(String[] args) {
        pool();
    }

    public static void pool(){
        //新建连接池
        JedisPoolConfig poolConfig = new JedisPoolConfig();
        //最大空闲数
        poolConfig.setMaxIdle(50);
        //最大等待毫秒数
        poolConfig.setMaxWaitMillis(20000);
        //使用配置创建连接池
        JedisPool pool = new JedisPool(poolConfig,"192.168.0.120");
        //从连接池中获单个连接
        Jedis jedis = pool.getResource();
        //如果需要密码
        //jedis.auth("密码");
        jedis.set("message","test message");
    }
}
```
 
 ## 在spring中使用Redis
 ```text
在spring中使用Redis，除了jedis.jar外，还需要下载spring-data-redis.jar。
值得注意的是jar包和spring版本兼容的问题，这里使用的版本略低
jedis.jar使用的2.9.0
spring-data-redis.jar使用的是1.7.2
spring版本是4.3.2
将这些jar包导入项目后，就可以使用spring提供的rRedistemplate操作Redis，
只是在使用前，需要针对spring提供的方案进行配置。
具体如下：
  1）使用spring配置edisPoolConfig对象
```
```xml
    <!--    配置jedisPoolConfig对象-->
    <bean id="poolConfig" class="redis.clients.jedis.JedisPoolConfig">
        <!--        最大空闲连接数-->
        <property name="maxIdle" value="50"/>
        <!--        最大连接数-->
        <property name="maxTotal" value="100"/>
        <!--        最大等待时间(毫秒)-->
        <property name="maxWaitMillis" value="20000"/>
    </bean>
```
```text
这里就设置连接池的配置，继续往下配置。
在使用spring提供的RedisTemplate之前需要配置spring锁提供的连接工厂，在spring-data-redis方案中提供了4重工厂
1）JredisConnectionFactory
2）JedisConnectionFactory
3）LettuceConnectionFactory
4）SrpConnectionFactory

虽然使用哪种实现工厂都可以的，但是要根据环境进行测试，已验证使用哪个方案的性能最佳。
无论如何他们都是接口RedisConnectionFactory的实现类，更多的时候通过接口定义他们，才具有接口适用性特性的。
当前的项目，使用应用最广泛的JedisConnectionFactory。
```
2)使用spring配置jedisConnectionFactory
```xml
    <!--    配置 JedisConnectionFactory 工厂bean-->
    <bean id="connectionFactory" class="org.springframework.data.redis.connection.jedis.JedisConnectionFactory">
        <property name="hostName" value="192.168.0.120"/>
        <property name="poolConfig" ref="poolConfig"/>
    </bean>
```
```text
属性配置
hostName：代表的是服务器，默认值是localhost，所以如果是本机可以不配置
port：代表接口端口，默认是6379
password：代表密码，在需要密码连接Redis的场合需要配置
poolConfig：是连接池配置对象，可以设置连接池的属性
```
```text
注意：普通的连接使用没有办法把java对象直接存入Redis，需要提供序列化方案，将对象序列化，然后使用Redis存储。
在获取序列化内容之后，在通过转换，变为java对象，spring模板中提了封装的方案，在他内部提供了RedisSerializer
接口(org.springfranework.data.redis.serializer.RedisSerializer)和一些实现类。
可以选择spring提供了序列法方案，也可以去实现spring-data-redis中定义的RedisSerializer接口。
```

```text
在spring中提供了以下几种实现RedisSerializer的接口的序列化器。

1）GenericJackson2JsonRedisSerializer，通用的使用Json2.jar包，将Redis对象的序列化器。
2）Jackson2JsonRdisSerializer<T>，通过Jackson2.jar包提供的序列化进行转换。
3）JdkSerializationRedisSerializer<T>,使用JDK序列化器进行转化
4）OxmSerializer,使用SpringO/X对象Object和XML相互转换
5）StringRedisSerializer，使用字符串进行序列化
6）GenericToStringSerializer，通过通用字符串序列化进行相互转换

使用他们就能把对象进行序列化存储到Redis中，也可以把Redis存储的内容转换为java对象，为此Spring提供了RedisTemplate还有两个属性
1）keySerializer————键序列化器
2）valueSerializer——值序列化器。
```
 ```text
了解这些后，就可以配置RedisTemplate了。
假设选用StringRedisSerializer作为Redis的key序列化器，而使用JdkSerializationRedisSerializer作为其value的序列化器，
配置文件如下编写：
```
```xml
    <bean id="jdkSerializationRedisSerializer" class="org.springframework.data.redis.serializer.JdkSerializationRedisSerializer"/>
    
    <bean id="stringRedisSerializer" class="org.springframework.data.redis.serializer.StringRedisSerializer"></bean>
    
    <bean id="redisTemplate" class="org.springframework.data.redis.core.RedisTemplate">
        <property name="connectionFactory" ref="connectionFactory"/>
        <property name="keySerializer" ref="stringRedisSerializer"/>
        <property name="valueSerializer" ref="jdkSerializationRedisSerializer"/>
    </bean>     
```

```text
  举例：新建一个角色对象，使用Redis保存对象代码如下：
```
```java
public class Role implements Serializable {

    private static final long serialVersionUID = 4586752982488029699L;

    private long id;

    private String roleName;

    private String note;
    
    /** getter setter */
}
```
```text
  因为要序列化对象，所以需要实现Serializable接口，表明他能够序列化，而serialVersionUID代表序列化的版本号

  使用上述的配置方式来配置application.xml就可以使用RedisTemplate来保存这个对象了，代码如下：
```
```java
     ApplicationContext context = new ClassPathXmlApplicationContext("classpath:application.xml");
     TestObj testObj = (TestObj) context.getBean("testObj");
     System.out.println(testObj);

     RedisTemplate redisTemplate = context.getBean(RedisTemplate.class);
     Role role = new Role();
     role.setId(1l);
     role.setRoleName("role_name_1");
     role.setNote("note_1");
     redisTemplate.opsForValue().set("role_1",role);

     Role role1 = (Role) redisTemplate.opsForValue().get("role_1");
     System.out.println(role1);
```

```text
  set和get方法看起来很简单，他可能就来自于同一个Redis连接池的不同链接。
  为了使所有的操作都来自于同一个链接，可以使用SessionCallback或者RedisCallback这两个接口，
  RedisCallback是比较底层的封装，使用起来不是很友好。所以更多的时候回使用SessionCallback这个接口，
  通过这个接口就可以吧多个命令放入到同一个Redis连接中去执行，代码如下：
```
```java
    ApplicationContext context = new ClassPathXmlApplicationContext("classpath:application.xml");
    RedisTemplate redisTemplate = context.getBean(RedisTemplate.class);

    Role role = new Role();
    role.setId(2l);
    role.setRoleName("role_name_2");
    role.setNote("note_2");

    SessionCallback callback = new SessionCallback<Role>() {
        @Override
        public Role execute(RedisOperations redisOperations) throws DataAccessException {
            redisOperations.boundValueOps("role_2").set(role);

            return (Role) redisOperations.boundValueOps("role_2").get();
        }
    };

    Role saveRole = (Role) redisTemplate.execute(callback);
    System.out.println(saveRole);
```

 ## Redis的五种常用数据类型
 
|  数据类型   |   数据类型存储的值   |  说明    |
| ---- | ---- | ---- |
|  STRING(字符串)   |   可以是保存字符串，整数和浮点数   |   可以对字符串进行操作，比如增加自负或者求子串，如果是整数或者浮点数，可以实现计算，比如自增等   |
|  LIST(列表）    |  它是一个链表，他的每一个节点都包含一个字符串    |  Redis支持从链表的两端记性插入或者弹出节点，或者通过偏移对它进行裁剪；还可以读取一个或者多个节点，根据条件删除或者查找节点等    |
|  SET（集合）    |  它是一个收集器，但是是无需的，在它里面每一个元素都是你一个字符串，而且是独一无二，各不相同的    |  可以新增，读取，删除单个元素；检测到一个元素是否在集合中；计算他和其他结合的交集，并集，和差集等；随机从集合读取元素    |
|  HASH（哈希）    | 它类似于java语言中的Map，是一个键值对应的无序列表     |  可以增删改查单个键值对，可以可以获取所有的键值对    |
|  ZSET（有序集合）    | 它是一个有序的集合，可以包含字符串，整数浮点数，分值(score)，元素的排序是依据分值的大小来决定的     | 可以增删改查元素，更具分值的范围或者成员来获取对应的元素     |


## Redis数据结构和常用命令

- ### Redis字符串数据结构和常用命令【STRING】
```text
字符串是Redis最基本的数据结构，它将以一个键和一个值存储于Redis内部，它犹如Java的Map结构，让Redis通过键值找值。
```

字符串的一些基本命令

|命令|说明|备注|
|----|----|----|
|set key value|设置键值对|最常用的写入命令|
|get key |通过键取值|最常用的读取命令|
|del key|通过key，删除键值对|删除命令，返回删除数，注意，它是一个通用的命令，换句话说在其他数据结构中，也可使使用它|
|strlen key|求key指向字符串的长度|返回长度|
|getset key value|修改原来key的对应值，并将旧值返回|如果原来的值为空，则返回空，并设置新值|
|getrange key start end|获取子串|及字符串的长度=为len，把字符串看做一个数组，而Redis是以0开始计数的，所以start和end的取值范围是0~len-1|
|append key value|将新的字符串value，追加到原来key指向的字符串末|返回指向新字符串的长度|

Spring操作Redis
    1）配置Spring关于Redis字符串的运行环境
```xml
<!--    配置jedisPoolConfig对象-->
    <bean id="poolConfig" class="redis.clients.jedis.JedisPoolConfig">
<!--        最大空闲连接数-->
        <property name="maxIdle" value="50"/>
<!--        最大连接数-->
        <property name="maxTotal" value="100"/>
<!--        最大等待时间(毫秒)-->
        <property name="maxWaitMillis" value="20000"/>
    </bean>

<!--    配置 JedisConnectionFactory 工厂bean-->
    <bean id="connectionFactory" class="org.springframework.data.redis.connection.jedis.JedisConnectionFactory">
        <property name="hostName" value="192.168.0.120"/>
        <property name="poolConfig" ref="poolConfig"/>
    </bean>

<!--    配置springRedisTemplate-->
<!--   java对象是无法直接存储到redis的，需要进行序列化然后在存储到redis，Spring的redisTemplate，在这里可以定义序列化-->
    <bean id="jdkSerializationRedisSerializer" class="org.springframework.data.redis.serializer.JdkSerializationRedisSerializer"/>
    <bean id="stringRedisSerializer" class="org.springframework.data.redis.serializer.StringRedisSerializer"></bean>
    <bean id="redisTemplate" class="org.springframework.data.redis.core.RedisTemplate">
        <property name="connectionFactory" ref="connectionFactory"/>
        <property name="keySerializer" ref="stringRedisSerializer"/>
        <property name="valueSerializer" ref="stringRedisSerializer"/>
    </bean>
```
  2）测试代码，以及注意事项
```text
  注意，这里给Spring的RedisTemplate的键值学历恶化器设置为了String类型，所以他就是一种字符串的操作。
  假设把这段Spring的配置代码保存为一个独立为文件application.xml，那么可以按照以下代码进行测试：
```
```java
    ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath:application.xml");
    RedisTemplate redisTemplate = context.getBean(RedisTemplate.class);

    //设值
    redisTemplate.opsForValue().set("key1","value1");
    redisTemplate.opsForValue().set("key2","value2");

    //通过key获取值
    String key1 = (String) redisTemplate.opsForValue().get("key1");
    System.out.println("通过key获取值："+key1);

    //通过key删除值
    redisTemplate.delete("key1");

    //求长度
    Long size = redisTemplate.opsForValue().size("key2");
    System.out.println("通过key获取value的字符长度："+size);

    //设置新值并返回旧值
    String key2 = (String) redisTemplate.opsForValue().getAndSet("key2", "new_value2");
    System.out.println("设置新值，返回旧值："+key2);

    //求子串
    String key21 = redisTemplate.opsForValue().get("key2", 0, 3);
    System.out.println("通过key，截取value的字符，[0-3]："+key21);

    //追加字符串到末尾，返回新字符串长度
    Integer append = redisTemplate.opsForValue().append("key2", "_app");
    System.out.println("追加字符串到末尾，返回新字符串的长度："+append);
```
  3）Redis支持的简单运算
 ```text
Redis提供了对整数和浮点型数字的功能，如果字符串是数字(整数或者浮点数)，那么Redis还能支持简单的运算，不过它的运算能力比较弱。
常用的加减法运算如下：
```

|命令|说明|备注|
|----|----|----|
|incr key|在原字段上加1|只能对整数操作|
|incrby key increment|在原字段上加上整数(increment)|只能对整数操作|
|decr key|在原字段上减1|只能对整数操作|
|decrby key increment|在原字段上键入整数(increment)|只能对整数操作|
|incrbyfloat key increment |在原字段上机上浮点数(increment)|可以操作浮点数或者整数|


- ### Redis哈希数据结构和常用命令【HASH】

```text
  Redis中哈希结构就如同java的map一样，一个对象里面有许多键值对，它是特别适合存储对象的。
  在Redis中，hash是一个String类型额field和value的映射表，因此存储的数据实际在Redis内存中都是一个个字符串而已。
```
1) Redis Hash结构常用命令

|命令|说明|备注|
|----|----|----|
|hdel key field1 [field2 ...]|删除hash结构中某个(些)字段|可以进行多个字段的删除|
|hexists key field|判断hah结构中是否存在field字段|存在返回1，否则返回0|
|hgetall key|获取所有hash结构中的键值|返回键和值|
|hincrby key field increment|指定给hash结构中某一个字段加上一个整数(increment)|要求该字段也是整数字符串|
|hincrbyfloat key field increment|指定给hash结构中某一个字段加上一个浮点数(increment)|要求该字段是一个数字型字符串|
|hkeys key|返回hash中所有的键|-|
|hlen key|返回hash中键值对的数量|-|
|hmget key field1 [field2 ...]|返回hash中指定的键值，可以是多个|一次返回值|
|hmset key field1 value1 [field2 ...]|hash结构设置多个键值对|-|
|hset key field value|在hash结构中设置键值对|单个设置|
|hsetnx key field value|当hash结构中不存子啊对应的键，才设置值|-|
|hvals key|获取hash结构中所有的值|-|

```text
  在Redis中的hash结构和字符串有着比较明显的不同。
首先，命令都是以h开头，代表操作的是hash结构。
其次，大多数命令多了一个层级field，这时hash结构的一个内部键，也就是说Redis需要通过key索引到对应的hash结构，早图片名股股票field来确定使用hash结构的哪个键值对。

Redis hash相关的命令，这里需要注意的是：
 1） hash结构的到校，如果hash结构是一个很大的键值对，name使用它要十分注意，尤其是关于hkeys，hgetall，hvals等返回所有hash机构数据的名利，会造成大量数据的读取。这里需要考虑性能和读取数据大小对jvm的影响。
 2） 对于数字的命令hincrby而言，要求存储的也是整形的字符串，对hincrbyfloat而言，则要求使用浮点数或者整数，否则命令会失效。
```
2） 使用Spring操作Redis的hash结构
```text
使用Spring去操作Redis的hash结构，由于spring对Redis进行封装，所以有必要RedisTemplate的配置项进行修改，下面是RedisTemplate的配置：
```
```xml
    <bean id="jdkSerializationRedisSerializer" class="org.springframework.data.redis.serializer.JdkSerializationRedisSerializer"/>
    <bean id="stringRedisSerializer" class="org.springframework.data.redis.serializer.StringRedisSerializer"></bean>
    <bean id="redisTemplate" class="org.springframework.data.redis.core.RedisTemplate">
        <property name="connectionFactory" ref="connectionFactory"/>
        <property name="keySerializer" ref="stringRedisSerializer"/>
        <property name="valueSerializer" ref="stringRedisSerializer"/>
        <property name="defaultSerializer" ref="stringRedisSerializer"/>
    </bean>
```

```text
  这段代码参考上面String操作的，所以做了一定的省略。这里是把Spring提供的RedisTemplate的默认序列化器(dufaultSerializer)修改为了字符串序列化器。
因此，在Spring对hash结构的操作中会设计map等其他类的操作，所以需要明确它的规则。这里只是指定了默认的学历恶化器，如果想为hash结构指定序列化器，可以
使用RedisTemplate提供的两个属性hashKeySerializer和hashValueSerializer，来为hash结构的field和value指定序列化器。

测试代码如下：
```
```java
 public void testHash(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath:application.xml");

        RedisTemplate template = context.getBean(RedisTemplate.class);

        String key = "hash";

        Map<String,String> map = new HashMap<>();
        map.put("f1","val1");
        map.put("f2","val2");

        //相当于hmset命令
        template.opsForHash().putAll(key,map);

        //相当于hset命令
        template.opsForHash().put(key,"f3","6");
        printValueForHash(template,key,"f3");

        //相当于hexists key filed命令
        boolean exists = template.opsForHash().hasKey(key,"f3");
        System.out.println(exists);

        //相当于hgetall命令
        Map keyValMap = template.opsForHash().entries(key);

        //相当于hincrby命令
        template.opsForHash().increment(key,"f3",2);
        printValueForHash(template,key,"f3");

        //相当于bincrbyfloat命令
        template.opsForHash().increment(key,"f3",0.88);
        printValueForHash(template,key,"f3");

        //相当于hvals命令
        List values = template.opsForHash().values(key);

        //相当于hkeys命令
        Set keys = template.opsForHash().keys(key);

        List<String> fieldList = new ArrayList<>();
        fieldList.add("f1");
        fieldList.add("f2");
        //相当于hmge命令
        template.opsForHash().multiGet(key,keys);

        //相当于hsetnx命令
        boolean success = template.opsForHash().putIfAbsent(key,"f4","val4");
        System.out.println(success);

        //相当于hdel命令
        Long result = template.opsForHash().delete(key,"f1","f2");
        System.out.println(result);


    }

    public static void printValueForHash(RedisTemplate template,String key,String filed){
        System.out.println(template.opsForHash().get(key, filed));
    }
```
```text
这里可能需要注意以下几点内容：
1） hmset命令：在java的API中，是使用map保存多个键值对在先的。
2） hgetall命令：会返回所有的键值对，并保存到一个map对象中，如果hash结构很大，那么要考虑它对JVM的内存影响
3） hincrby和hincrbyFloat：都是采用increment方法，Spring会识别它具体时间使用何种方法。
4） redisTemplate.opsForHash().putAll(key,map)：相当于hkeys命令，使用map，
    由于配置了默认的序列化为字符串，所以它也只会用字符串进行转化，这样才能执行对应的数值加法，如果使用了其他的序列化器，则后面的命令可能会抛出异常。
5） redisTemplate.opsForHash().value(key)：相当于命令hvals，他会返回所有的值，并保存到List对象中，
    而redisTemplate.opsForHash().keys(key)：相当于hkeys命令，他会获取所有的键，并保存到一个Set对象中。
6） 在使用大的hash结构式，需要考虑返回的数据大小，以避免返回太多的数据，引发JVM内存溢出或者Redis的性能问题。
```
- ### Redis链表数据结构和常用命令【LINK】
```text
  链表结构是Redis中一个常用的结构，它可以存储多个字符串，而且是有序的。
  Redis链表是双向的，因此既可以从左到右，也可以从右到左遍历它存储的节点。
 
  链表的优势在于插入和删除的便利，因为链表的数据节点是分配在不同的内存区域，并不连续，只是根据上一个节点保存下一个节点的顺序来索引而已，无需移动元素。

  链表结构的使用是需要注意场景的，对于那些经常需要对数据进行插入和删除的列表数据使用它是十分方便的，因为他可以在不移动其他节点的情况下完成插入和删除，而对于需要经常查找的，使用它性能并不佳，他只能从左到右或者从右到左的查找和对比。

  因为是双向链表结构，所以Redis链表命令分为做操作和有操作两种命令，做操作就意味着是从左到右，有操作就意味着是从右到左。
```
  1) Redis关于链表的命令
 
 |命令|说明|备注|
 |----|----|----|
 |lpush key node1 [node2 ...]|把节点node1加入到链表最左边|如果node1、node2、nodeN这样加入，name链表从左到右的顺序是nodeN、node2、node1|
 |rpush key node1 [node2 ...]|把节点node1加入链表的最右边|如果node1、node2、nodeN这样加入，name链表从左到右的顺序是node1、node2、nodeN|
 |lindex key index|读取下标为index的节点|返回节点字符串，从0开始|
 |llen key|求链表的长度|返回链表节点数|
 |lpop key|删除左边第一个节点，并将其返回|-| 
 |rpop key|善友右边第一个节点，并将其返回|-|
 |linsert key before\|after pivot node|插入一个节点node，并且可以指定在值为pivot的节点前面(before)，或者后面(after)|如果list不存在，则标错，如果没有值为对应pivot的，也会插入失败返回-1|
 |lpushx list node|如果存在key为list的链表，则插入节点node，并且作为从左到右的第一个节点|如果list不存在，则失败|
 |rpushx list node|如果存在key为list的链表，则插入节点node，并且作为从左到右的最后一个节点|如果list不存在，则失败|
 |lrange list start end|获取链表list从start下标到end下标的节点值|包含start和end下标的值|
 |lrem list count value|如果count为0，则删除所有值等于value的节点，如果count不是0，则先对count去绝对值，假设几倍abs，然后从左到右删除不大于abs个等于value的节点|注意count为整数，如果是负数，则Redis会先曲奇绝对值，然后传递到后台| 
 |lset key index node|设置列表下标为index的节点的值为node|-|
 |ltrim key start stop|修剪链表，指标刘从start到stop的区间的节点，其余的都删除掉|包含start和end下标的节点会保留|
 
 ```text
  上面锁具额就是常用的链表命令，其中以"L"开头的代表作操作，以"R"开头的代表有操作。

  对于很多歌节点同时操作的，需要考虑其花费的时间，链表数据结构对于查找而言并不适合大数据，而Redis也给了比较灵活的另类对其进行操作。

  Redis关于链表，如果有大量数据操作的时候，要考徐插入和删除的内容的大小，因为这将使十分消耗性能的命令，会导致Redis服务器卡顿，对于不允许出现卡顿的一些服务器，可以进行分批操作，以避免出现卡顿。

  需要指出的是，之前这些操作链表的命令都是进程不安全的，当客户端A操作这些命令的时候，客户端B也可能操作同一个链表，这样就会操作成编发数据安全和一致性问题，尤其是操作的链表数据链不小的时候，常常会遇到这样的问题。

  为了克服这些问题，Redis提供了链表的阻塞命令，他们在运行时，会给链表加锁，以保证操作链表的命令安全性
  命令如下：
```

|命令|说明|备注|
|----|----|----|
|blpop key timeout|移出并获取链表的第一个元素，如果列表没有元素会阻塞列表，直到等待超时或者发现可弹出的元素为止|相当于lpop命令，它的操作是进程安全的|
|brpop key timeout|移出并获取链表最后一个元素，如果列表没有元素会则色列表，直到等待超时或者发现可弹出的元素为止|相当于rpop命令，它的操作是进程安全的|
|rpoplpush key src dest|按从左到右的顺序，将一个链表的最后一个元素移出，并插入到目标链表的最左边|不能设置超时时间|
|brpoplpush key src dest timeout|按从左到右的顺序，将一个链表的最后一个元素移出，并插入到目标链表的最左边，并可以设置超时时间|可以设置超时时间|

```text
  当使用上面这些命令时，Redis会对对应的链表加锁，加锁的结果就是其他的进程不能在读取或者写入该链表，只能等待命令结束。
 
  加锁的好处可以保证在多线程并发环境中数据的一致性，保证一些重要数据的一致性，比如账户的金额，上平的数量。

  不过在保证这些的同事也要付出其他线程等待、线程环境切换等代价，者将使得系统的并发能力下降。
```
 2）Spring操作Redis的链表结构
  
  测试辅助方法
  ```java
    public static void printList(RedisTemplate redisTemplate,String key){
        //链表长度
        Long size = redisTemplate.opsForList().size(key);
        //获取整个链表的值
        List<String> range = redisTemplate.opsForList().range(key, 0, size);

        for (String v : range){
            System.out.println(v);
        }

    }
```
  
  spring操作Redis的常用操作
  ```java
     /**
     * RedisTemplate对list的常用操作
     * */
    public static void testList(){
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:application.xml");

        RedisTemplate redisTemplate = applicationContext.getBean(RedisTemplate.class);

        try {
            redisTemplate.delete("list");

            //从左侧插入一个节点
            redisTemplate.opsForList().leftPush("list","node3");

            List<String> nodeList = new ArrayList<>();

            for (int i = 2; i >= 1 ; i--) {
                nodeList.add("node"+i);
            }
            //从左侧插入一个list集合
            redisTemplate.opsForList().leftPushAll("list",nodeList);
            //从右侧插入一个节点
            redisTemplate.opsForList().rightPush("list","node4");

            //获取下表为0 的节点
            String index = (String) redisTemplate.opsForList().index("list", 0);
            System.out.println(index);
            //获取链表长度
            Long size = redisTemplate.opsForList().size("list");
            System.out.println(size);

            //从左边弹出一个节点
            String leftPop = (String) redisTemplate.opsForList().leftPop("list");
            //从右边弹出一个节点
            String rightPop = (String) redisTemplate.opsForList().rightPop("list");

            //注意，需要使用底层命令才能使用linsert命令
            //在node2节点前，添加一个before_node节点
            redisTemplate.getConnectionFactory().getConnection().lInsert("list".getBytes(Charset.forName("utf-8")),
                    RedisListCommands.Position.BEFORE,
                    "node2".getBytes(Charset.forName("utf-8")),
                    "before_node".getBytes(Charset.forName("utf-8")));

            //在node2节点后，添加一个before_node节点
            redisTemplate.getConnectionFactory().getConnection().lInsert("list".getBytes(Charset.forName("utf-8")),
                    RedisListCommands.Position.AFTER,
                    "node2".getBytes(Charset.forName("utf-8")),
                    "after_node".getBytes(Charset.forName("utf-8")));

            //判断list是否存在，如果存在则从左边插入head节点
            redisTemplate.opsForList().leftPushIfPresent("list","head");
            //判断list是否存在，如果存在则从右边插入end节点
            redisTemplate.opsForList().leftPushIfPresent("list","end");

            //从左到右，或者从下标从 0 到 10 的节点元素
            List<String> valueList = redisTemplate.opsForList().range("list",0,10);

            //在链表左边插入三个职位node的节点
            nodeList.clear();
            for (int i = 1; i <=3 ; i++){
                nodeList.add("node");
            }

            //从左到右删除至多三个node节点
            redisTemplate.opsForList().remove("list",3,"node");

            //给链表下表为0的节点设置新值
            redisTemplate.opsForList().set("list",0,"new_head_value");

        }catch (Exception e){
            e.printStackTrace();
        }

        //打印链表数据
        printList(redisTemplate,"list");
    }
``` 
  spring操作Redis的阻塞操作 
```java
     /**
     * Spring对Redis的阻塞命令操作
     * */
     public static void testBList(){
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:application.xml");

        RedisTemplate redisTemplate = applicationContext.getBean(RedisTemplate.class);
        //清空链表，适用于反复测试
        redisTemplate.delete("list1");
        redisTemplate.delete("list2");

        //初始化链表
        List<String> nodeList = new ArrayList<>();

        for (int i = 1; i <= 5; i++){
            nodeList.add("node"+i);
        }

        redisTemplate.opsForList().leftPushAll("list1",nodeList);
        //Spring使用参数超时时间作为阻塞命令区分，等价于blpop命令，并且可以设置时间参数
        redisTemplate.opsForList().leftPop("list1",1, TimeUnit.SECONDS);

        //spring使用超时时间作为阻塞命令区分，等价于brpop命令，并且可以设置时间参数
        redisTemplate.opsForList().rightPop("list1",1,TimeUnit.SECONDS);

        nodeList.clear();
        //初始化链表二
        for (int i = 1; i <= 3; i++){
            nodeList.add("data"+i);
        }

        redisTemplate.opsForList().leftPushAll("list2",nodeList);

        //相当于rpoplpush，弹出list1最右边的节点，插入到list2的最左边
        redisTemplate.opsForList().rightPopAndLeftPush("list1","list2");
        //相当于brpoplpush，注意，在spring中使用超时参数区分
        redisTemplate.opsForList().rightPopAndLeftPush("list1","list2",1,TimeUnit.SECONDS);

        //打印链表数据
        printList(redisTemplate,"list1");

        System.out.println("=========");

        printList(redisTemplate,"list2");

    }
```

- ### Redis集合数据结构和常用命令【SET】
```text
  Redis的集合不是一个线性结构，而是一个哈希表结构，它的内部会根据hash分子来存储和查找数据。

  因为采用哈希表结构，所以对于Redis集合的插入，删除和查找的复杂度都是O(1)。

  需要注意三点
    1）对于集合而言，它的每一个元素都是不能重复的，当出入相同记录的时候都会失败。
    2）集合是无序的。
    3）集合的每一个元素都是String数据结构类型。
 
  Redis的集合可以对于不同的结合进行操作，比如求出两个或者以上的集合的交集、并集、差集。
```
 1）集合【SET】命令
 
 |命令|说明|备注|
 |----|----|----|
 |sadd key member1 [member1 ...]|给键为key的集合增加成员|可以同时增加多个|
 |scard key|统计键为key的集合成员数|-|
 |sdiff key1 [key2]|找出两个集合的差集|参数如果是单个key，那么Redis就会返回这个key的所有元素|
 |sdiffstore des key1 [key2]|先按diff命令的规则，找出key1和key2两个集合的差集，然后将其保存到des集合中|-|
 |sinter key1 [key2]|求key1和key2两个集合的交集|参数如果是单个key，那么Redis会返回这个key的所有元素|
 |sinterstore des key1 [key2]|先按sinter命令的规则，找出key1key2两个集合的交集，然后保存到des中|-|
 |sismember key member|判断member是否为key的结合的成员|如果是返回1，否则返回0|
 |smembers key|返回集合所有成员|如果数据量大，需要考虑迭代遍历的问题|
 |smove src des member|-|
 |spop key|随机弹出一个元素|将member从集合src迁移到集合des中|注意其随机性，因为集合是无序的|
 |srandmember key [count]|随机返回集合中一个或者多个元素，count为限制返回总数，如果count为负数，则先求其绝对值|count为整数，如果填默认为1，如果count大于集合总数，则返回整个集合|
 |srem key member1 [menber2 ...]|移出集合中的元素，可以使多个元素|对于很大的集合可以通过他删除部分元素，避免删除大量数据引发Redis停顿|
 |sunion kye1 [key2 ...]|求两个集合的并集|参数如果是单key，nameRedis就会返回这个可以的所有元素|
 |sunionstore des key1 key2|限制性sunion命令求出并集，然后保存到键为des的集合中|-|
 
 
 2）Spring操作Redis集合SET
 ```java
 public static void testSet(){

        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:application.xml");

        RedisTemplate redisTemplate = applicationContext.getBean(RedisTemplate.class);


        Set set = null;

        //将元素加入列表
        redisTemplate.boundSetOps("set1").add("v1","v2","v3","v4","v5","v6");
        redisTemplate.boundSetOps("set2").add("v0","v2","v4","v6","v8");

        //求集合长度
        Long set1 = redisTemplate.opsForSet().size("set1");

        //求差集
        set = redisTemplate.opsForSet().difference("set1","set2");

        //求并集
        set = redisTemplate.opsForSet().intersect("set1","set2");

        //判断是否集合中元素
        boolean exists = redisTemplate.opsForSet().isMember("set","v1");

        //获取结集合所有元素
        set = redisTemplate.opsForSet().members("set1");

        //从集合中随机弹出一个元素
        String val = (String) redisTemplate.opsForSet().pop("set1");

        //随机获取一个集合的元素
        val = (String) redisTemplate.opsForSet().randomMember("set1");

        //随机获取2个结合的元素
        List<String> list =  redisTemplate.opsForSet().randomMembers("set1",2L);

        //删除一个集合的元素，参数可以是多个
        redisTemplate.opsForSet().remove("set1","v1");

        //求两个集合的并集
        redisTemplate.opsForSet().union("set1","set2");

        //求连个集合的差集，并保存到集合diff_set中
        redisTemplate.opsForSet().differenceAndStore("set1","set2","diff_set");

        //求两个集合的交集，并保存到集合inter_set中
        redisTemplate.opsForSet().intersectAndStore("set1","set2","inter_set");

        //求两个集合的并集，并保存到集合union_set中
        redisTemplate.opsForSet().unionAndStore("set1","set2","union_set");
    }
```
 
- ### Redis有序集合数据结构和常用命令【ZSET】
```text
  有序集合和集合类似，只是说它是有序的，和无需集合的主要区别在每一个元素除了值以外，它还会多一个分数。

  分数是一个浮点数，在java中使用双精度表示的，根据分数，Redis就可以支持对分数从小到大或者从大到小的排序。

  这里和无需集合一样，对于每一个元素都是唯一的，范式对于不同元素而言，它的分数可以一样。

  元素也是String数组类型，也是一种集合hash储存的结构。

  集合是通过哈希表实现的，所以添加、删除、查询的复杂度都是O(1)。

  
  有序集合是用来key标示它是属于哪个集合，依赖分数进行排序，所以值和分数是必须的，而实际上不仅可以对分数进行排序，在满足一定的条件下，也可以对值进行排序。
```


 1）Redis对有序集合【ZSET】的操作命令
```text
  有序集合和无序集合的命令是捷径的，只是在这些命令的基础上，会增加对于排序的操作，这些使我们在使用的时候需要注意的细节。

  下面是有序集合的一些常用命令：
```


|命令|说明|备注|
|----|----|----|
|zadd key score1 value1 [score2 value2 ...]|向有序集合key，增加一个或者多个成员|如果不存在对应的key，则创建键为key的有序集合|
|zcard key|获取有序集合的成员数|-|
|zcount key min max|根据分数返回对应的成员列表|min为最小值，max为最大值，默认为保安min和max值，采用数学区间表示的方法如果需要不报案，则在分数前加入"("，注意不支持"["表示|
|zincrby key increment member|给有序集合成员为member的分数增加increment|-|
|zinterstore desKey numKeys key1[key2 key3 ...]|求多个有序集合的交集，并将结果保存到desKey中|numKeys是一个整数，表示有多少个有序集合|
|zlexcount key min max|求有序集合key成员值在min和max的范围|这里范围为可以的成员值，Redis借助数据区间的表示方式，"["表示包含该值，"("表示不包含该值|
|zrange key start stop [withscores]|按照分值大小(从小到大)返回成员，加入start和stop参数可以截取某一段返回。如果输入可选项withscores，则联通分数一起返回|这里记集合最大长度为len，则Redis会将集合排序后，形成一个从0到len-1的下标，然后根据start和stop控制下表(包含start和stop)返回|
|zrank key member|按照从大到小求有序集合的排行|排名第一的为0，排名第二的为1|
|zrangebylex key min max [limit offset count]|根据值的大小，从小到大排序，min为最小值，max为最大值；limit选项可选，当Redis求出范围集合后，会生产下标0到N，然后根据偏移量offset和限定返回数count，返回对应的成员|这里的范围为key的成员值，Redis借助数学区间的表示方法，"["表示包含该值，"("表示不包含|
|zrangebyscorekey min max [withscores limit offset count]|根据分数大小，从小到大求取范围，选项withscores和limit请参考zrange命令和zrangebylex说明|根据分析求取集合的范围，这里默认包含min和max，如果不想包含，则在参数前加入"("，注意不支持"["表示|
|zremrangebysocre key start top|数据分数区间进行删除|按照score进行排序，然后删除0到len-1的下标，然后根据start和stop进行删除，Redis借助数学区间的表示方式，"["表示包含该值，"("表示不包含该值|
|zremrangerank key start stop|按照分数排行从小到大排序删除，从0开始计算|-|
|zrenrangebylex key min max|按照值的分布进行删除|-|
|zrevange key start stop [withscores]|从大到小的按分数排序吗，参数请参见zrange|与zrange相同，只是排序是从大到小|
|zrevrangescore key max min [withscores]|从大到小的安分数排序，参数请参见zrangebyscore|与zrangebycore相同，只是排序是从大到小|
|zrevrank key member|按照从大到小的顺序，求出元素的排行|排名第一为0，第二位1|
|zscore key member|返回成员的分数值|返回成员的分数|
|zunionstore desKey numKeys key1 [key2 key3 key4 ...]|求多个有序集合的并集，其中numKeys是有序集合的个数|-|

```text
  在对有序集合、下标、区间的表示方法进行操作的时候，需要十分小心命令，注意它是操作分数还会值，稍有不慎就会出现问题。

  这里命令比较多，也有些命令比较难使用，在使用的时候，务必小心，不过好在使用zset的频率不是太高。
```

  2）Spring对有序集合的封装
 ```text
  在Spring中使用Redis的有序集合，需要注意的是Spring对Redis有序集合的元素的值和分数范围(Range)和限制(Limit)进行了封装，在使用Spring操作有序结合前，还需要进一步了解它的封装。

  一个主要的接口———TypedTuple，台式一个普通的接口，而是一个内部接口，它是org.springframework.data.redis.core.ZSetOperations接口的内部接口，
 它定义了两个方法如下：
```
```java
 public interface TypedTuple<V> extends Comparable<ZSetOperations.TypedTuple<V>> {
        V getValue();

        Double getScore();
    }
```
```text
  这里的getValue()是获取值，而getScore()是获取分数的，但是他只是一个接口，而不是一个实现类吗。
  Spring-data-redis提供了一个默认的实现类————DefaultTypedTuple，同样它是实现TypedTuple接口，
  在默认的情况下Spring就会把带有分数的有序集合的值和分数封装到这个类中，这样就可以通过这个类对象读取对应的值和分数了。

  Spring不仅对有机集合的元素封装，而且对范围也进行了封装，方便使用，
  它是使用接口：org.springframework.data.redis.connection.RedisZSetCommands下的内部类Range进行封装的，
  它有一个静态的range方法，使用它就可生成一个Range对象了，只要清楚Range对象的几个方法，就可以更好的操作。

  具体方法，如下(伪代码)：
```
```java
  //设置大于等于min
  public Range gte(Object min);

  //设置大于min
  public Range gt(Object min);

  //设置小于等于max
  public Range lte(Object max);

  //设置小于max
  public Range le(Object max);
```
```text
 上面四个方法就是最常用的范围方法。

 接下来是Spring对limit操作的封装。

 同样的，它也是一个接口的内部类：org.springframework.data.redis.connection.RedisZSetCommands;
 它是一个简单地POJO对象，存在两个属性，以及属性getter和setter方法
 
 代码如下：
```
```java
  public static class Limit {
         int offset;
         int count;
  }
```
```text
  通过属性名称很容易知道 ：offset代表从第几个开始截取
                      : count代表显示返回的总数量
```
 
```text
  在使用spring-data-redis对有序集合的操作之前，需要先声明一下，下面的测试代码，操作的都是字符串而非java对象
  所以，要把RedisTemplate的keySerializer和valueSerializer属性都修改为字符串序列化器StringRedisSerializer
  如果需要测试其他的数据类型，可以自定义序列化器，只需要更改配置文件即可。

  以下是Spring对Redis中ZSET的相关代码
```
 
```java

    //辅助方法
    /**
     *  打印TypedTuple集合
     * @param set
     * */
    public static void printTypedTuple(Set<ZSetOperations.TypedTuple> set){
        if (set == null || set.isEmpty()) {
            return;
        }

        Iterator iterator = set.iterator();
        while (iterator.hasNext()){
            ZSetOperations.TypedTuple val = (ZSetOperations.TypedTuple) iterator.next();
            System.out.println("{value="+val.getValue()+".score="+val.getScore()+"}\n");
        }
    }

    /**
     * 打印普通集合
     * */
    public static  void printSet(Set set){
        if (set == null || set.isEmpty()){
            return;
        }
        Iterator iterator = set.iterator();
        while (iterator.hasNext()){
            Object val = iterator.next();
            System.out.println(val+"\t");
        }
        System.out.println();
    }
```
```java
    public static void testZSet(){
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:application.xml");
        RedisTemplate redisTemplate = applicationContext.getBean(RedisTemplate.class);

        //Spring提供的TypedTuple来操作有序集合
        Set<ZSetOperations.TypedTuple> set1 = new HashSet<>();
        Set<ZSetOperations.TypedTuple> set2 = new HashSet<>();

        int j = 9;
        for (int i = 1; i <= 9; i++){
            j--;
            //计算分数和值
            Double score1 = Double.valueOf(1);
            String value1 = "x"+i;

            Double score2 = Double.valueOf(j);
            String value2 = j % 2== 1 ? "y"+j : "x"+j;
            //使用spring提供的默认TypedTuple---DefaultTypedTuple
            ZSetOperations.TypedTuple typedTuple1 = new DefaultTypedTuple(value1,score1);
            set1.add(typedTuple1);
            ZSetOperations.TypedTuple typedTuple2 = new DefaultTypedTuple(value2,score2);
            set2.add(typedTuple2);
        }
        //将元素插入有序集合zset1
        redisTemplate.opsForZSet().add("zset1",set1);
        redisTemplate.opsForZSet().add("zset2",set2);

        //统计总数
        Long size = null;
        size = redisTemplate.opsForZSet().zCard("zset1");
        System.out.println(size);

        //计分数为score，name下面的方法就是求 3<=score<=6的元素
        Long count = size = redisTemplate.opsForZSet().count("zset1", 3, 6);

        //从下标一开始截取五个元素，但是不返回分数，每一个元素是string
        Set set = null;
        set = redisTemplate.opsForZSet().range("zset",1,5);
        printSet(set);

        //截取集合中所有元素，并且对象按分数排序，并返回分数，每一个元素是TypedTuple
        set = redisTemplate.opsForZSet().rangeWithScores("zset1",0,-1);
        printTypedTuple(set);

        //将zset1和zset2两个集合的交集放入集合inter_zset
        size = redisTemplate.opsForZSet().intersectAndStore("zset1","zset2","inter_zset");

        //区间
        RedisZSetCommands.Range range = RedisZSetCommands.Range.range();
        range.lt("x8");//小于
        range.gt("x1");//大于
        set = redisTemplate.opsForZSet().rangeByLex("zset1",range);
        printSet(set);
        //限制返回个数
        RedisZSetCommands.Limit limit = RedisZSetCommands.Limit.limit();
        //限制返回个数
        limit.count(4);
        //限制从第5个开始截取
        limit.offset(5);
        //求区间内的的元素，并限制返回4条
        set = redisTemplate.opsForZSet().rangeByLex("zset1",range,limit);


        //求排行，排名第1返回0，第2返回1
        Long rank = redisTemplate.opsForZSet().rank("zset1","x4");
        System.out.println("rank="+rank);

        //删除元素返回个数
        size = redisTemplate.opsForZSet().remove("zset1","x5","x6");
        System.err.println("delete="+size);

        //按照排行删除从0开始算起，这里将删除排名第2和第3的元素
        redisTemplate.opsForZSet().removeRange("zset1",1,2);

        //获取所有的集合元素和分数，以-1代表全部元素
        set = redisTemplate.opsForZSet().rangeWithScores("zset2",0,-1);
        printTypedTuple(set);

        //删除指定元素
        size = redisTemplate.opsForZSet().remove("zset2","y5","y3");
        System.err.println(size);

        //给集合中的一个元素的分加上11
        Double db1 = redisTemplate.opsForZSet().incrementScore("zset1","x1",11);
        System.out.println(db1);

        redisTemplate.opsForZSet().removeRangeByScore("zset",1,2);
        set = redisTemplate.opsForZSet().reverseRangeWithScores("zset2",1,10);
        printTypedTuple(set);
    }
```
