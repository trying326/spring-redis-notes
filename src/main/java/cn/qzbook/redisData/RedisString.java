package cn.qzbook.redisData;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.redis.core.RedisTemplate;

/**
 * @Description
 * @Author zc
 * @Date 2020/11/7 下午8:50
 */
public class RedisString {

    public static void main(String[] args) {
        RedisString string = new RedisString();
        string.string();
    }

    /**
     * 通过RedisTemplate操作String(字符型)数据
     * */
    private void string(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath:application.xml");

        RedisTemplate redisTemplate = context.getBean(RedisTemplate.class);

        //设值
        redisTemplate.opsForValue().set("key1","value1");
        redisTemplate.opsForValue().set("key2","value2");

        //通过key获取值
        String key1 = (String) redisTemplate.opsForValue().get("key1");
        System.out.println("通过key获取值："+key1);

        //通过key删除值
        redisTemplate.delete("key1");

        //求长度
        Long size = redisTemplate.opsForValue().size("key2");
        System.out.println("通过key获取value的字符长度："+size);

        //设置新值并返回旧值
        String key2 = (String) redisTemplate.opsForValue().getAndSet("key2", "new_value2");
        System.out.println("设置新值，返回旧值："+key2);

        //求子串
        String key21 = redisTemplate.opsForValue().get("key2", 0, 3);
        System.out.println("通过key，截取value的字符，[0-3]："+key21);

        //追加字符串到末尾，返回新字符串长度
        Integer append = redisTemplate.opsForValue().append("key2", "_app");
        System.out.println("追加字符串到末尾，返回新字符串的长度："+append);

        /**
         * 则是主要的目的只是在spring上操作redis的键值对，其他操作就等同于上面的命令一样。
         * 在spring中，RedisTemplate.opsForValue()所返回的对象可以操作简单的键值对，可以使字符串，也可以是对象
         * 具体的数据依据配置文件中配置的序列化方案。
         * 当前操作的都是字符并没有涉及到对象，value的序列方案，使用的string<property name="valueSerializer" ref="stringRedisSerializer"/>
         * */
    }

}
