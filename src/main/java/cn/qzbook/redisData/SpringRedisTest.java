package cn.qzbook.redisData;

import cn.qzbook.pojo.Role;
import cn.qzbook.test.TestObj;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.core.RedisOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.SessionCallback;


/**
 * @Description
 * @Author zc
 * @Date 2020/11/2 下午8:07
 */
public class SpringRedisTest {

    public static void main(String[] args) {
        SpringRedisTest test = new SpringRedisTest();
//        test.init();
          test.sessionCallback();
    }

    public void init(){

        ApplicationContext context = new ClassPathXmlApplicationContext("classpath:application.xml");
        TestObj testObj = (TestObj) context.getBean("testObj");
        System.out.println(testObj);

        RedisTemplate redisTemplate = context.getBean(RedisTemplate.class);
        Role role = new Role();
        role.setId(1l);
        role.setRoleName("role_name_1");
        role.setNote("note_1");
        redisTemplate.opsForValue().set("role_1",role);

        Role role1 = (Role) redisTemplate.opsForValue().get("role_1");

        System.out.println(role1);
    }

    /**
     * RedisTemplate使用的连接池，在操作redis的时候，一个连接只能使用一次
     * 所以不能保证多次操作使用的使用同一个连接
     * 在特殊情况下，如果需要使用同一个连接来操作redis数据的时候
     * 可以使用SessionCallback接口，来保证多次操作redis使用的是同一个连接
     * */
    public void sessionCallback(){

        ApplicationContext context = new ClassPathXmlApplicationContext("classpath:application.xml");

        RedisTemplate redisTemplate = context.getBean(RedisTemplate.class);

        Role role = new Role();
        role.setId(2l);
        role.setRoleName("role_name_2");
        role.setNote("note_2");

        SessionCallback callback = new SessionCallback<Role>() {
            @Override
            public Role execute(RedisOperations redisOperations) throws DataAccessException {
                redisOperations.boundValueOps("role_2").set(role);

                return (Role) redisOperations.boundValueOps("role_2").get();
            }
        };

        Role saveRole = (Role) redisTemplate.execute(callback);
        System.out.println(saveRole);

    }


}
