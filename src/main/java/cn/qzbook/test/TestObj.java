package cn.qzbook.test;

/**
 * @Description
 * @Author zc
 * @Date 2020/11/2 下午8:43
 */
public class TestObj {

    public String name;

    public int age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "TestObj{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
