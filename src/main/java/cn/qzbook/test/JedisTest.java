package cn.qzbook.test;

import redis.clients.jedis.Jedis;

/**
 * @Description
 * @Author zc
 * @Date 2020/11/2 下午7:40
 */
public class JedisTest {

    public static void main(String[] args) {
        Jedis jedis = new Jedis("192.168.0.120",6379);

        int i = 0;
        try {
            long start = System.currentTimeMillis();
            while (true){
                long end = System.currentTimeMillis();
                if (end - start >= 1000){
                    break;
                }
                i++;
                jedis.set("test"+i,i+"");
            }
        }finally {
            jedis.close();
        }

        System.out.println("redis每秒操作："+i+"次");
    }

//    public static void main(String[] args) {
//        Jedis jedis = new Jedis("192.168.0.120",6379);
////        for (int i = 3;i < 112; i++) {
////            jedis.del("test"+i);
////        }
//
//    }

}
