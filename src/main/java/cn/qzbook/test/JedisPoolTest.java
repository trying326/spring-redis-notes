package cn.qzbook.test;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 * @Description
 * @Author zc
 * @Date 2020/11/2 下午7:52
 */
public class JedisPoolTest {

    public static void main(String[] args) {
        pool();
    }

    public static void pool(){
        //新建连接池
        JedisPoolConfig poolConfig = new JedisPoolConfig();
        //最大空闲数
        poolConfig.setMaxIdle(50);
        //最大等待毫秒数
        poolConfig.setMaxWaitMillis(20000);
        //使用配置创建连接池
        JedisPool pool = new JedisPool(poolConfig,"192.168.0.120");
        //从连接池中获单个连接
        Jedis jedis = pool.getResource();
        //如果需要密码
        //jedis.auth("密码");
        jedis.set("message","纵情山河万里，肆意九州五岳");

    }

}
