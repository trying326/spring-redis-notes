package cn.qzbook.pojo;

import java.io.Serializable;

/**
 * @Description
 * @Author zc
 * @Date 2020/11/2 下午9:47
 */
public class Role implements Serializable {

    private static final long serialVersionUID = 4586752982488029699L;

    private long id;

    private String roleName;

    private String note;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public String toString() {
        return "Role{" +
                "id=" + id +
                ", roleName='" + roleName + '\'' +
                ", note='" + note + '\'' +
                '}';
    }
}
